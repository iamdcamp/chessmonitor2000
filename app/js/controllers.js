/**
 * Created by dpcampbe on 4/23/2014.
 */
'use strict';

/* Controllers */

var cm2000App = angular.module('cm2000App', []);

cm2000App.controller('Cm2000Ctrl',
	function($scope) {

		$scope.players = [];
		$scope.curVal = null;
		$scope.uidToRm = null;

		$scope.getUid = function() {
			return Date.now();
		}

		$scope.addPlayer = function(newPlayer) {
			console.log("trying to add uid: " + newPlayer);
				if(newPlayer) {
					var found = false;
					for(var i = 0; i < $scope.players.length; i++) {
						console.log("In array: " + $scope.players[i].name + ", to match: " + newPlayer);
						if(newPlayer === $scope.players[i].name) {
							console.log("match found");
							found = true;
						}
						break;
					}
					if(!found) {
						$scope.players.push({"name": newPlayer, "uid": $scope.getUid()});
					}
					$scope.curVal = null;
				}
		};

		$scope.removePlayer = function(uid) {
			console.log("trying to remove uid: " + uid);
			if(uid) {
				for(var i = 0; i < $scope.players.length; i++) {
					if(uid === $scope.players[i].uid) {
						$scope.players.splice(i, 1);
					}
					break;
				}
			}
		};


	});
